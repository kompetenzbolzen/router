#!/bin/bash

cd build/root || exit 1
find . | cpio -o -H newc | gzip > ../initramfs.cpio.gz

du -h ../initramfs.cpio.gz
