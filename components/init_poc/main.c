#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <poll.h>

#include <sys/wait.h>

#include <dirent.h>

#include <sys/mount.h>

#include <dirent.h>

#include <sys/reboot.h>

#define ASSERT_ERRNO(e){\
	if(e!=0){printf("Errno %i: %s\n",errno,strerror(errno));}\
}

//Needs NULL as last arg!!
int run_start(char* _prog, char* _args[])
{
	pid_t pid = fork();

	if( pid < 0 )
		return 1;
	else if (pid > 0)
		return 0;


	// TODO Why do you exist?
	setsid();
	if(fork())
		exit(0);

	close(STDOUT_FILENO);
	close(STDIN_FILENO);
	close(STDERR_FILENO);

	execvp(_prog, _args);

	exit(1);
}

//Need NULL as last arg!!
int run_stdio(char* _prog, char* _args[])
{	
	pid_t pid;
	if((pid = fork()) == -1)
		return 2;

	if (pid == 0) {
		execvp(_prog, _args);

		//Error handling
		int err = errno;
		printf("execvp failed with %i: %s\n", err, strerror(err));

		exit(1);
	} else {
		int status;
		waitpid(pid, &status, 0);
		return 0;
	}
}

void ls(char* _path) {
	struct dirent *files;
	DIR *dir = opendir(_path);
	if (dir == NULL){
	   printf("Directory cannot be opened!\n" );
	   return;
	}
	while ((files = readdir(dir)) != NULL)
		printf("%s\n", files->d_name);
	closedir(dir);
}

int main(int argc, char* argv[]) {
	ASSERT_ERRNO(mount("devtmpfs","/dev","devtmpfs",0,NULL));
	ASSERT_ERRNO(mount("proc","/proc","proc",0,NULL));
	ASSERT_ERRNO(mount("sysfs","/sys","sysfs",0,NULL));
	ASSERT_ERRNO(mount("tmpfs","/tmp","tmpfs",0,NULL));

	ls("/proc");

	setenv("PATH","/bin",1);
	setenv("LD_LIBRARY_PATH","/lib",1);

	printf("Started\n");

	char* args[] = {"/bin/netman",NULL};
	run_stdio("/bin/netman", args);

	reboot(RB_POWER_OFF);
	return 0;
}
