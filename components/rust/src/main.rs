mod system;
mod misc;
mod process;

use system::mount;

fn main() {
    println!("init.rs");

    let mnts = [
        ("sysfs","/sys","sysfs",0,""),
        ("proc","/proc","proc",0,""),
        ("devtmpfs","/dev","devtmpfs",0,""),
        ("tmpfs","/tmp","tmpfs",0,""),
        ("/dev/sda","/mnt","ext4",system::MS_NOEXEC,""),
    ];

    let envs = [
        ("PATH","/bin"),
        ("LD_LIBRARY_PATH","/lib"),
    ];

    // Ignore SIGCHLD to avoid zombies
    unsafe {
        libc::signal(libc::SIGCHLD, libc::SIG_IGN);
    }

    for mnt in mnts {
        println!("mount {}", mnt.0);
        mount(mnt.0,mnt.1,mnt.2,mnt.3,mnt.4).expect(mnt.1);
    }

    println!("\nENVIRONMENT");
    for env in envs {
        println!("{}={}", env.0, env.1);
        std::env::set_var(env.0,env.1);
    }

    println!("Running netman");
    let mut proc = process::Process::new("netman", "netman", &[], false);
    proc.run();

    while proc.running() {
        proc.tick();
        unsafe{libc::sleep(1);}
    }
    println!("Done!");

    //let paths = std::fs::read_dir("/dev/").unwrap();
    //for path in paths {
    //            println!("Name: {}", path.unwrap().path().display());
    //}
    //unsafe{libc::sleep(20);}

    println!("Unmounting filesystems");
    // TODO umount()
    for mnt in mnts {
        println!("unmount {}", mnt.0);
        system::umount(mnt.1).expect(mnt.1);
    }

    println!("\nShutting down...");
    system::shutdown().expect("Shutdown");
}
