#![allow(dead_code)]
extern crate libc;

use std::ffi::{
    CString,
};

use misc::{ErrnoResult, result_errno};

// Export needed constants
pub use self::libc::{
    MS_NOEXEC,
};

// TODO: data?
pub fn mount(source: &str, target: &str, fstype: &str, flags: u64, _data: &str) -> ErrnoResult<()> {
    let c_src = CString::new(source).expect("source: CString failed");
    let c_target = CString::new(target).expect("target: CString failed");
    let c_fstype = CString::new(fstype).expect("fstype: CString failed");

    let ret = unsafe{ libc::mount(c_src.as_ptr(), c_target.as_ptr(), c_fstype.as_ptr(), flags, std::ptr::null()) };

    match ret {
        0 => Ok(()),
        _ => result_errno(),
    }
}

pub fn umount(target: &str) -> ErrnoResult<()> {
    let c_target = CString::new(target).expect("target: CString failed");

    let ret = unsafe{ libc::umount(c_target.as_ptr()) };

    match ret {
        0 => Ok(()),
        _ => result_errno(),
    }
}

pub enum ForkResult {
    Parent(u32),
    Child()
}
pub fn fork() -> ErrnoResult<ForkResult> {
    let ret = unsafe{ libc::fork() };

    match ret {
        1.. => Ok(ForkResult::Parent(ret as u32)),
        0   => Ok(ForkResult::Child()),
        _   => result_errno(),
    }
}

pub fn shutdown() -> ErrnoResult<()> {
    let ret = unsafe{ libc::reboot(libc::RB_POWER_OFF) };

    match ret {
        0 => Ok(()),
        _ => result_errno(),
    }
}
