#![allow(dead_code)]
use std::process::{
    Command,
    Child,
    Stdio,
};
//use std::io::{
////   write,
////  read
//};

pub enum ProcessState<'a> {
    Running,
    Exited(Option<i32>),
    Failed(&'a str),
    Stopped,
}

pub struct Process<'a> {
    name: &'a str,
    exe: &'a str,
    args: &'a[&'a str],
    child: Option<Child>,
    state: ProcessState<'a>,
    should_restart: bool,
    restart_count: u32,
}

struct ProcessManager<'a> {
    procs: std::vec::Vec<Process<'a>>,
}

impl<'a> Process<'a> {
    pub fn new(name: &'a str, exe: &'a str, args: &'a[&'a str], should_restart: bool) -> Process<'a> {
        Process {
            name,
            exe,
            args,
            child: None,
            state: ProcessState::Stopped,
            should_restart,
            restart_count: 0
        }
    }

    pub fn run(&mut self) {
        match Command::new(self.exe).args(self.args)
            .stdin(Stdio::null()).stdout(Stdio::piped()).stderr(Stdio::piped())
            .spawn() {
            Ok(c)  => {
                self.child = Some(c);
                self.state = ProcessState::Running;
            },
            Err(_) => {
                self.child = None;
                self.state = ProcessState::Failed("spawn failed");
            },
        }
    }

    pub fn tick(&mut self) {
        match &mut self.child {
            Some(c) => self.state = match c.try_wait() {
                Ok(None) => ProcessState::Running,
                Ok(Some(s)) => ProcessState::Exited(s.code()),
                Err(_) => ProcessState::Failed("try_wait() failed")
            },
            None => {println!("No child");}, // TODO remove
        }
    }

    pub fn running(&self) -> bool {
        match &self.state {
            ProcessState::Running => true,
            _       => false,
        }
    }
}

//impl ProcessManager {
//    fn add_process(&mut self, name: str, exe: str, args: [str], should_restart: bool) {
//        //let mut Process
//    }
//}
