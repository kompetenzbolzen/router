/*
 * src/iface.rs
 * (c) 2022 Jonas Gunz <himself@jonasgunz.de>
 * License: All rights reserved.
 */

use std::convert::TryInto;
use std::str::FromStr;

use misc;

pub fn test_interface() {
    unsafe {test_interface_us()}
}

// NOTE This function may return without freeing its resources upon failing.
unsafe fn test_interface_us() {
    let sin: *mut libc::sockaddr_in = libc::malloc(std::mem::size_of::<libc::sockaddr_in>()) as *mut libc::sockaddr_in;
    let ifr: *mut libc::ifreq = libc::malloc(std::mem::size_of::<libc::ifreq>()) as *mut libc::ifreq;

    let iface = "eth0";
    let c_iface = std::ffi::CString::new(iface).expect("CString failed");

    let newip = "172.16.1.2";
    //let c_newip = std::ffi::CString::new(nweip).expect("CString failed");

    let newmask = "255.255.0.0";

    libc::strncpy((*ifr).ifr_name.as_mut_ptr(), c_iface.as_ptr(), libc::IFNAMSIZ);

    let socket = libc::socket(libc::AF_INET, libc::SOCK_DGRAM, 0);


    // NOTE: Watch out here!
    // This convert is a musl thing...
    // musl ioctl: int ioctl(int fd, int req, ...)
    // glibc     : int ioctl(int fd, unsigned long req, ...)
    if libc::ioctl(socket, libc::SIOCGIFFLAGS.try_into().unwrap() , ifr) < 0 {
        println!("Error: {}", misc::strerror());
        return;
    }

    if ( (*ifr).ifr_ifru.ifru_flags & libc::IFF_UP as i16 ) == 0x0 {
        println!("IF is down. Bringing it up");

        (*ifr).ifr_ifru.ifru_flags &= libc::IFF_UP as i16;

        if libc::ioctl(socket, libc::SIOCSIFFLAGS as i32 , ifr) < 0 {
            println!("Error: {}", misc::strerror());
            return;
        }

        println!("OK!")
    }

    (*sin).sin_family = libc::AF_INET as u16;

    //NOTE: libc does not have inet_aton...
    //libc::inet_aton(c_newip.as_ptr(), &(*sin).sin_addr.s_addr);
    let addr = std::net::Ipv4Addr::from_str(newip).expect("Failed to parse IP");
    let mut addr_t: u32 = 0;
    let octets = addr.octets();

    for i in 0..4 {
        println!("{}", i);
        addr_t += (octets[i] as u32) << 8*(i);
        println!("octet {}", (octets[i] as u32) << 8*(i));
    }
    (*sin).sin_addr.s_addr = addr_t;

    libc::memcpy(std::ptr::addr_of_mut!((*ifr).ifr_ifru.ifru_addr) as *mut libc::c_void, sin as *mut libc::c_void, std::mem::size_of::<libc::sockaddr>());

    if libc::ioctl(socket, libc::SIOCSIFADDR.try_into().unwrap() , ifr) < 0 {
        println!("Error: {}", misc::strerror());
        return;
    }

    // TODO Netmask

    let addr = std::net::Ipv4Addr::from_str(newmask).expect("Failed to parse netmask");
    let mut addr_t: u32 = 0;
    let octets = addr.octets();

    for i in 0..4 {
        println!("{}", i);
        addr_t += (octets[i] as u32) << 8*(i);
    }
    (*sin).sin_addr.s_addr = addr_t;

    libc::memcpy(std::ptr::addr_of_mut!((*ifr).ifr_ifru.ifru_netmask) as *mut libc::c_void, sin as *mut libc::c_void, std::mem::size_of::<libc::sockaddr>());

    if libc::ioctl(socket, libc::SIOCSIFNETMASK.try_into().unwrap() , ifr) < 0 {
        println!("Error: {}", misc::strerror());
        return;
    }
    libc::close(socket);
    libc::free(ifr as *mut libc::c_void);
    libc::free(sin as *mut libc::c_void);
}
