extern crate libc;

use std::ffi::{
    CStr
};

pub fn strerror() -> &'static str {
    unsafe {
        let c_errstr = libc::strerror(*(libc::__errno_location)());
        CStr::from_ptr(c_errstr).to_str().expect("strerror: Failed to convert from CStr")
    }
}

// 'static should be OK here, since the string can be modified,
// but is never freed (-> strerror(3))
// TODO does it really tho?
pub type ErrnoResult<T> = Result<T,&'static str>;
pub fn result_errno<T>() -> ErrnoResult<T>{
    Err(strerror())
}
