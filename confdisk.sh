#!/bin/bash

BUILDDIR="build"

test -d "$BUILDDIR" || exit 1

CMD="$1"
shift


create() {
	dd if=/dev/zero of="$BUILDDIR/conf.raw" bs=1M count=64
	#mkfs.fat -n CONFIG -F 32 "$BUILDDIR/conf.raw"
	mkfs.ext4 -L config "$BUILDDIR/conf.raw"
}

copy() {
	which fuse2fs > /dev/null 2>&1 || exit 69
	DIR=$(mktemp -d)

	fuse2fs "$BUILDDIR/conf.raw" "$DIR" -o fakeroot
	cp "$@" "$DIR"
	fusermount -u "$DIR"
}

case $CMD in
	create)
		create;;
	copy)
		copy "$@";;
	*)
		echo args;;
esac
