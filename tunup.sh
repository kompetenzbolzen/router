#!/bin/sh

SET_IP="172.16.1.1/16"

ip link set "$1" up
ip address add "$SET_IP" dev "$1"

exit 0
