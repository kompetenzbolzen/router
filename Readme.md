# router

NetDEV:
https://linuxtips.ca/index.php/2022/09/13/set-interface-ip-address-from-c-in-linux/

## Links

https://www.kernel.org/doc/html/latest/index.html

Kernel networking config
https://www.kernel.org/doc/html/latest/networking/index.html

Netlink https://www.kernel.org/doc/html/latest/networking/ethtool-netlink.html
Sysctl IP https://www.kernel.org/doc/html/latest/networking/ip-sysctl.html
rtnetlink https://linux.die.net/man/7/rtnetlink
https://stackoverflow.com/questions/17900459/how-to-set-ip-address-of-tun-device-and-set-link-up-through-c-program
netdevice https://www.man7.org/linux/man-pages/man7/netdevice.7.html

## Building

`./build.sh build` to trigger builds

`./build.sh` to just install
