#!/bin/bash

function do_build(){
	test "$DOBUILD" = "build"
	return $?
}

BUILDNR=$(git describe --always --dirty)

DOBUILD="$1"

BUILDDIR="$(pwd)/build"

mkdir -p "$BUILDDIR"/root/{bin,lib,dev,sys,proc,tmp,mnt}

#ln -s usr/bin build/root/bin
#ln -s usr/lib build/root/lib


(
cd components/linux || exit 1
do_build && make -j$(nproc)
cp "arch/x86/boot/bzImage" "$BUILDDIR"
)

(
cd components/rust-libc || exit 1
do_build && rustc --crate-name libc --target x86_64-unknown-linux-musl src/lib.rs \
	--cfg libc_union -o "$BUILDDIR/liblibc.rlib"
)

(
cd components/rust || exit 1
do_build && make
cp init "$BUILDDIR/root/"
)

(
cd components/netman || exit 1
do_build && make
cp netman "$BUILDDIR/root/bin"
)

(
cd components/musl || exit 1

do_build && ./configure --prefix="$BUILDDIR/musl/" --exec-prefix="" \
	--disable-static --syslibdir="/lib" --enable-wrapper=disabled
do_build && make -j12
do_build && make install

cd "$BUILDDIR" || exit 1

cp musl/lib/libc.so root/lib/
ln -s libc.so root/lib/ld-musl-x86_64.so.1
)

CHECK_FOR=( lib/libc.so init lib/ld-musl-x86_64.so.1 )
for f in ${CHECK_FOR[@]}; do
	test -e "build/root/$f" || echo "WARNING: $f not in rootfs"
done

./pack.sh
