#!/bin/bash

git submodule init && \
	git submodule update

CHECK_FOR=( gcc make bc cpio gzip qemu-system-x86_64 )
for f in ${CHECK_FOR[@]}; do
	which "$f" > /dev/null 2>&1 || echo "WARNING: command $f not in PATH"
done
