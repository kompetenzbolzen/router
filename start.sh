#!/bin/bash

KERNEL="bzImage"
INITRAMFS="initramfs.cpio.gz"
KERNELARGS="console=ttyS0 panic=1"
MEMORY="1024M"

cd build || exit 1

NIC=()
if [ $UID -eq 0 ]; then
	modprobe tun || exit 1
	NIC=( "-nic" "tap,script=$(pwd)/../tunup.sh,downscript=/bin/true" )
fi

HDA=()
if [ -f conf.raw ]; then
	HDA=( "-hda" "./conf.raw" )
fi

qemu-system-x86_64 -enable-kvm -nographic -no-reboot \
	-kernel "$KERNEL" -initrd "$INITRAMFS" \
	-append "$KERNELARGS" \
	-m "$MEMORY" \
	"${NIC[@]}" \
	"${HDA[@]}"
